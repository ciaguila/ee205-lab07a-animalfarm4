///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 4
///
/// @file maint.cpp
/// @version 1.0
///
///
///
/// @author @todo Christopher Aguilar <ciaguila@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   March 2021
///////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include "node.hpp"
#include "list.hpp"

using namespace std;
using namespace animalfarm;

int main(){

Node node;
cout << "poof" << endl;

SingleLinkedList list;
cout << "poof list" << endl;

 cout << boolalpha << list.empty() << endl;
}
