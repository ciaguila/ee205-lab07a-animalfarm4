#include <iostream>
#include <string>
#include <cstdlib>
#include <ctime>
#include <random>

#include "af.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "mammal.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"
#include "fish.hpp"
#include "bird.hpp"

#define RANDOM_NAME Animal::getRandomName()
#define RANDOM_COLOR Animal::getRandomColor()
#define RANDOM_GENDER Animal::getRandomGender()
#define RANDOM_BOOL Animal::getRandomBool()
#define RANDOM_NAME Animal::getRandomName()
#define RANDOM_WEIGHT Animal::getRandomWeight( 0, 20)

using namespace std;

namespace animalfarm {


Animal* AnimalFactory::getRandomAnimal() {
   Animal* newAnimal = NULL;
      
      random_device rd;
      mt19937 mt(rd());
      uniform_real_distribution<double> dist(0, 6);

      int i = dist(mt); 
      //printf("DEBUG: Entered Animal Factory %d \n", i);
 switch (i){
  case 0: newAnimal = new Cat (RANDOM_NAME, RANDOM_COLOR, RANDOM_GENDER); break;
  case 1: newAnimal = new Dog ( RANDOM_NAME, RANDOM_COLOR, RANDOM_GENDER); break;
  case 2: newAnimal = new Nunu ( RANDOM_BOOL, RED, RANDOM_GENDER); break;
  case 3: newAnimal = new Aku ( RANDOM_WEIGHT, SILVER, RANDOM_GENDER); break;
  case 4: newAnimal = new Palila ( RANDOM_NAME, YELLOW, RANDOM_GENDER); break;
  case 5: newAnimal = new Nene ( RANDOM_NAME, BROWN, RANDOM_GENDER); break;
 }
 
return newAnimal;
};

} // namespace animalfarm

