///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file bird.cpp
/// @version 1.0
///
/// Exports data about all birds
///
/// @author @todo Christopher Aguilar
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date Feb 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

#include "bird.hpp"

using namespace std;

namespace animalfarm {

void Bird::printInfo() {

   Animal::printInfo();
   cout << "   Feather Color = [" << colorName( featherColor ) << "]" << endl; // sends the featherColor enum to the color name string return, and returns the correct color string
   cout << "   Is Migratory= [" << boolalpha << isMigratory << "]" << endl; //prints out whether a bird is migratory, help within the bird data
}

//This is declared as a speak for all birds, overridden by any specific bird speak
const string Bird::speak() {
   return string( "Tweet" );
}



} // namespace animalfarm

