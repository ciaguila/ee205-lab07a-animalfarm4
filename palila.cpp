///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file palila.cpp
/// @version 1.0
///
/// Exports data about all palila birds
///
/// @author @todo Christopher Aguilar
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @todo Feb2021
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>

#include "palila.hpp"

using namespace std;

namespace animalfarm {

Palila::Palila( string whereFound, enum Color newColor, enum Gender newGender ) {
   gender = newGender;
   species = "Loxioides bailleui";    /// Hardcoded due to all palila being the same species
   featherColor = newColor;
  isMigratory = false; //Hardcoded as false due to all palila not being migratory
   whereF = whereFound; //This holder is used to pass data from the constructor to the printInfo

}


/// Print where our palila is found, and then forwards to the Bird Printinfo
void Palila::printInfo() {
   cout << "Palila" << endl;
   cout << "   Where found = [" << whereF << "]" << endl;
   Bird::printInfo();
}

} // namespace animalfarm


