
#pragma once

using namespace std;

#include <string>
#include "animal.hpp"
#include "node.hpp"

namespace animalfarm {

class AnimalFactory {

   public:
   static Animal* getRandomAnimal();
};


} // namespace animalfarm

