// Animal Farm 3
//
// Christopher Aguilar
//
// March 2021

#include <iostream>
#include <array>
#include <list>
#include <random>

#include <cstdlib>
#include <ctime>

#include "af.hpp"
#include "animal.hpp"
#include "mammal.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"
#include "fish.hpp"
#include "bird.hpp"

#include "list.hpp"
#include "node.hpp"

using namespace std;
using namespace animalfarm;

int main() {

   srand(time(NULL));
   cout << "Welcome to Animal Farm 4" << endl;

SingleLinkedList animalList; // Instantiate a SingleLinkedList
for( auto i = 0 ; i < 25 ; i++ ) {
animalList.push_front( AnimalFactory::getRandomAnimal() ) ;
//cout << "Debug element size:" << animalList.size() << endl;
}

cout << endl;
cout << "List of Animals" << endl;
cout << " Is it empty: " << boolalpha << animalList.empty() << endl;
cout << " Number of elements: " << animalList.size() << endl;

for( auto animal = animalList.get_first() // for() initialize
 ; animal != nullptr // for() test
 ; animal = animalList.get_next( animal )) { // for() increment

   cout << ((Animal*)animal)->speak() << endl;
}

while( !animalList.empty() ) {
Animal* animal = (Animal*) animalList.pop_front();
delete animal;
}

return 0;
}
