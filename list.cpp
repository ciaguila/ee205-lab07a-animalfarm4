///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 4
///
/// @file node.hpp
/// @version 1.0
///
///
///
/// @author @todo Christopher Aguilar <ciaguila@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   March 2021
///////////////////////////////////////////////////////////////////////////////

#include<iostream>

#include "list.hpp"
#include "node.hpp"

using namespace std;

namespace animalfarm{

const bool SingleLinkedList::empty() const{
      if(head == nullptr)
      {
         return true;
      }
      else
      {
         return false;
      }
   }

void SingleLinkedList::push_front(Node* newNode){

   Node *tmp = newNode;
   tmp->next = head;
   head = tmp;
   //cout << "pushfront" << endl;

}

Node* SingleLinkedList::pop_front() {
      //cout << "popfront" << endl;
      if(head == nullptr)
      {
         return nullptr;
      }

      Node *tmp = head;
      head = head->next;
      return tmp;
      
}

Node* SingleLinkedList::get_first() const {
   return head;
}

Node* SingleLinkedList::get_next( const Node* currentNode ) const {
   
   return (currentNode->next);
}

size_t SingleLinkedList::size() const{
  
   //cout << "getsize" << endl;
   Node* h = head;
   unsigned int size=0;
   while(h != NULL)
   {
      //cout << "sizeadd" << endl;
      size++;
      h = h->next;
   }
   return size;
}


} // namespace animalfarm

